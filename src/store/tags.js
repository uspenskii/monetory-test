import { defineStore } from "pinia";

export const useTags = defineStore("tags-store", {
  state: () => ({
    tags: [],
  })
});
