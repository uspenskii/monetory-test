import { defineStore } from "pinia";

export const useModal = defineStore("modal", {
  state: () => ({ state: '', alert: false }),
});