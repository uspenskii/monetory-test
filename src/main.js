import { createApp } from "vue";
import { createPinia } from "pinia";
import App from "./App.vue";

import "./assets/scss/reset.scss";
import "./assets/scss/variables.scss";
import "./assets/scss/global.scss";
import "./assets/scss/fonts.scss";

const pinia = createPinia();

createApp(App).use(pinia).mount("#app");
